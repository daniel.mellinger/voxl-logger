/*******************************************************************************
 * Copyright 2022 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <getopt.h>
#include <time.h>
#include <string.h>
#include <png.h>
#include <zlib.h>
#include <turbojpeg.h>

#include <cmath>
#include <fstream>
#include <thread>
#include <queue>

#include <modal_pipe_server.h>
#include <modal_start_stop.h>
#include <modal_pipe_interfaces.h>
#include <modal_json.h>

#include "common.h"
#include "log_defs.h"


using namespace std;

#define THROW(action, message)                                             \
{                                                                          \
	printf("ERROR in line %d while %s:\n%s\n", __LINE__, action, message); \
}

#define THROW_TJ(action) THROW(action, tjGetErrorStr2(tjInstance))

#define THROW_UNIX(action) THROW(action, strerror(errno))

#define MIN_CAM_PIPE_SIZE	(16*1024*1024) // 16MiB for now


typedef struct cam_data{
	camera_image_metadata_t meta;
	unsigned char* frame1;
	unsigned char* frame2;
} cam_data;

typedef struct channel_t{
	int running;
	int type;
	pthread_t thread_id;
	char out_pipe_path[MODAL_PIPE_MAX_PATH_LEN];
	char log_pipe_path[MODAL_PIPE_MAX_PATH_LEN];
	int n_total; // total samples as reported by the log file
	int n_read; // number of sample read including skipped samples

	// extra queue and thread for camera processing
	std::queue<std::shared_ptr<cam_data>> line_queue;
	pthread_t cam_prep_id;
	pthread_mutex_t line_mutex = PTHREAD_MUTEX_INITIALIZER;

	FILE* csv_fd;
	FILE* raw_fd;
} channel_t;

// array of each channel state
static channel_t c[PIPE_SERVER_MAX_CHANNELS];

// this is the complete path beginning with base_dir into which all log data
// in included, for example: /data/voxl-logger/log0001/
static char log_dir[256];
static char base_dir[128] = LOG_BASE_DIR;

static int n_ch;					// number of channels in log
static int en_debug;				// optional debug mode
static int auto_kill;
static int64_t log_start_time_ns;	// start time when the log was created
static int64_t replay_start_time_ns;	// start time when the replay starts

// printed if some invalid argument was given
static void _print_usage(void)
{
	printf("\n\
Replay a log\n\
\n\
-d, --debug                 enable verbose debug mode\n\
-h, --help                  print this help message\n\
-n, --number {number}       number of log to open, e.g. 32\n\
-p, --path {path}           complete path of log to open, e.g. /data/mylog/\n\
-y, --auto_kill             automatically kills servers that conflict with the log\n\
\n\
\n");
	return;
}


static int _parse_opts(int argc, char* argv[])
{
	static struct option long_options[] =
	{
		{"debug",       no_argument,       0, 'd'},
		{"help",        no_argument,       0, 'h'},
		{"number",      required_argument, 0, 'n'},
		{"path",        required_argument, 0, 'p'},
		{"auto_kill",   no_argument,       0, 'y'},
		{0, 0, 0, 0}
	};

	// we are going to set up the channels in sequence based on the order of
	// arguments as the user provided them. Start at 0 indicating the user
	// hasn't specified any channels yet. Once the first channel starts, this
	// will increase

	while(1){
		int len;
		int option_index = 0;
		int j = getopt_long(argc, argv, "dhn:p:y", long_options, &option_index);

		if(j == -1) break; // Detect the end of the options.

		switch(j){
		case 0:
			// for long args without short equivalent that just set a flag
			// nothing left to do so just break.
			if (long_options[option_index].flag != 0) break;
			break;

		case 'h':
			_print_usage();
			return -1;
			break;

		case 'd':
			printf("enabling debug mode\n");
			en_debug = 1;
			break;

		case 'n':
			if(log_dir[0]!=0){
				fprintf(stderr, "can't provide multiple logs to replay\n");
				return -1;
			}
			sprintf(log_dir, "%slog%04d/", base_dir, atoi(optarg));
			break;

		case 'p':
			if(log_dir[0]!=0){
				fprintf(stderr, "can't provide multiple logs to replay\n");
				return -1;
			}
			strcpy(log_dir,optarg);
			len = strlen(log_dir);
			if(len<1){
				fprintf(stderr, "provided log path too short\n");
				return -1;
			}
			if(log_dir[0]!='/'){
				fprintf(stderr, "log dirrectory path must be absolute and begin with '/'\n");
				return -1;
			}
			if(log_dir[len-1]!='/'){
				fprintf(stderr, "log dirrectory path must be absolute and end with '/'\n");
				return -1;
			}
			break;
		
		case 'y':
			printf("enabling auto kill\n");
			auto_kill = 1;
			break;

		default:
			_print_usage();
			return -1;
		}
	}

	if(log_dir[0]==0){
		fprintf(stderr, "ERROR: you must specify a log to replay\n");
		_print_usage();
		return -1;
	}

	return 0;
}


// convert the xyz intrinsic rotation sequence we use for camera extrinsics to
// a rotation matrix. units of radians
static void _tait_bryan_xyz_to_rotation_matrix(float tb[3], float R[3][3])
{
	const double cx = cos(tb[0]);
	const double sx = sin(tb[0]);
	const double cy = cos(tb[1]);
	const double sy = sin(tb[1]);
	const double cz = cos(tb[2]);
	const double sz = sin(tb[2]);
	const double cxcz = cx * cz;
	const double cxsz = cx * sz;
	const double sxcz = sx * cz;
	const double sxsz = sx * sz;

	R[0][0] =  cy * cz;
	R[0][1] = -cy * sz;
	R[0][2] =  sy;
	R[1][0] =  cxsz + sxcz * sy;
	R[1][1] =  cxcz - sxsz * sy;
	R[1][2] = -sx * cy;
	R[2][0] =  sxsz - cxcz * sy;
	R[2][1] =  sxcz + cxsz * sy;
	R[2][2] =  cx * cy;

	return;
}

// convert the zyx intrinsic rotation sequence we use for airframe rotation to
// a rotation matrix. units of radians
static void _tait_bryan_zyx_to_rotation_matrix(float tb[3], float R[3][3])
{
	double c1 = cos(tb[2]);
	double s1 = sin(tb[2]);
	double c2 = cos(tb[1]);
	double s2 = sin(tb[1]);
	double c3 = cos(tb[0]);
	double s3 = sin(tb[0]);

	R[0][0] = c1*c2;
	R[0][1] = (c1*s2*s3)-(c3*s1);
	R[0][2] = (s1*s3)+(c1*c3*s2);

	R[1][0] = c2*s1;
	R[1][1] = (c1*c3)+(s1*s2*s3);
	R[1][2] = (c3*s1*s2)-(c1*s3);

	R[2][0] = -s2;
	R[2][1] = c2*s3;
	R[2][2] = c2*c3;

	return;
}

/**
 * This is a blocking function which returns 0 if the user presses ENTER.
 * If ctrl-C is pressed it will quit the program
 */
static int continue_or_quit(void)
{
	if (auto_kill) return 0;
	// set stdin to non-canonical raw mode to capture all button presses
	fflush(stdin);
	if(system("stty raw")!=0){
		fprintf(stderr,"ERROR in continue_or_quit setting stty raw\n");
		return -1;
	}

	int ret;

	while(1){
		int c = getchar();
		// break if we read ctrl-c
		if(c==3 || c==24 || c==26){
			ret=-1;
			break;
		}
		if(c=='\r' || c=='\n'){
			ret = 0;
			break;
		}
	}

	fflush(stdin);

	// put stdin back to normal canonical mode
	if(system("stty cooked")!=0){
		fprintf(stderr,"ERROR in continue_or_quit setting stty cooked\n");
		return -1;
	}

	printf("\n");
	return ret;
}


static int64_t _correct_timestamp_ns(int64_t log_ts)
{
	int64_t ret = log_ts + (replay_start_time_ns - log_start_time_ns);
	if(ret<0){
		fprintf(stderr, "critical error calculating corrected timestamp\n");
		return -1;
	}
	return ret;
}

static void _wait_before_publish(int ch, int64_t publish_ts_ns)
{
	// see how long we need to wait before publishing this packet and sleep if
	// it's positive. If negative we are falling behind which is okay as long
	// as it's a little bit behind, real world data will always have delay.
	int64_t time_to_go = publish_ts_ns - _time_monotonic_ns();
	if(time_to_go>0){
		usleep(time_to_go/1000);
	}
	if(time_to_go<-100000000 && en_debug){
		fprintf(stderr, "Warning: channel %d falling behind\n", ch);
	}
	return;
}

// TODO, bundle up multiple imu samples since they are so fast.
static int _publish_imu(int ch, char* line)
{
	imu_data_t d;
	int ret;
	int index;
	int64_t timestamp_log;

	// scan data out of the line
	ret = sscanf(line, "%d,%ld,%f,%f,%f,%f,%f,%f,%f\n",\
	&index, &timestamp_log,\
	&d.accl_ms2[0],&d.accl_ms2[1],&d.accl_ms2[2],\
	&d.gyro_rad[0],&d.gyro_rad[1],&d.gyro_rad[2],\
	&d.temp_c);

	d.magic_number = IMU_MAGIC_NUMBER;

	// make sure all fields were populated
	if(ret!=9){
		fprintf(stderr, "failed to parse IMU csv line %d for channel %d\n", c[ch].n_read+1, ch);
		perror("error:");
		return -1;
	}

	// correct the timestamp to line up with current time and wait if necessary
	d.timestamp_ns = _correct_timestamp_ns(timestamp_log);
	_wait_before_publish(ch, d.timestamp_ns);

	// write to pipe!
	if(en_debug){
		printf("publishing to ch%d at %ldus an IMU packet with timestamp %ldus\n", ch, _time_monotonic_ns()/1000,d.timestamp_ns/1000);
	}
	pipe_server_write(ch, (char*)&d, sizeof(d));

	return 0;
}


static void _construct_cam_path(int ch, int i, char* path)
{
	sprintf(path, "%s%s%05d.png", log_dir, c[ch].out_pipe_path, i);
	return;
}

static void _construct_jpg_cam_path(int ch, int i, char* path)
{
	sprintf(path, "%s%s%05d.jpg", log_dir, c[ch].out_pipe_path, i);
	return;
}

static void _construct_stereo_path(int ch, int i, char* path_l, char* path_r)
{
	sprintf(path_l, "%s%s%05dl.png", log_dir, c[ch].out_pipe_path, i);
	sprintf(path_r, "%s%s%05dr.png", log_dir, c[ch].out_pipe_path, i);
	return;
}

static void _interweave_yuv(unsigned char *Y, unsigned char* U, unsigned char* V, int width, int height, unsigned char* output)
{
	int size = width * height * 1.5; 

	memcpy(output, Y, (width * height));

	int index = 0;
	for (int i = width*height; i < size; i++) {
		output[i] = U[index];
		i+=1;
		output[i] = V[index];
		index += 1;
	}
}

static int libpng_read_image(int format, bool is_16_bit, char image_path[512], png_bytep &buffer){
	/////////////////////////////////////////////////////////////////
	// libpng
	/////////////////////////////////////////////////////////////////
	png_image image;
	/* Initialize the 'png_image' structure. */
	memset(&image, 0, (sizeof image));

	image.version = PNG_IMAGE_VERSION;

	if (png_image_begin_read_from_file(&image, image_path) == 0) return -1;
	
	image.format = format;

	buffer = (png_bytep)malloc(PNG_IMAGE_SIZE(image));
	
	if (png_image_finish_read(&image, NULL, buffer, is_16_bit, NULL) == 0){
		if (buffer == NULL)
			png_image_free(&image);
		else 
			free(buffer);

		fprintf(stderr, "pngtopng: error: %s\n", image.message);
		return -1;
	}

	return 0;
}

static int turbojpeg_read_image(char image_path[512],int w, int h, unsigned char* output){
	if (output == nullptr) return -1;

	int ret = 0;

	char* memblock;
	streampos file_size;
	std::ifstream input( image_path, std::ios::in|std::ios::binary|std::ios::ate );
	if (input.is_open()){
		file_size = input.tellg();
		memblock = new char [file_size];
		input.seekg (0, ios::beg);
		input.read (memblock, file_size);
		input.close();
	}
	else return -1;

	/* BEGIN JPEG COMPRESS */
	tjhandle tjInstance = NULL;

	unsigned char Y[w*h];
	unsigned char U[(w*h)/4];
	unsigned char V[(w*h)/4];

	unsigned char* planes[3] = { Y, U, V };
	int flags = 0;
	// speed up flags
	flags |= TJFLAG_FASTUPSAMPLE;
	flags |= TJFLAG_FASTDCT;

	if ((tjInstance = tjInitDecompress()) == NULL){
		THROW_TJ("initializing compressor");
		ret = -1;
	}

	if(!ret && tjDecompressToYUVPlanes(tjInstance, (unsigned char*)memblock, file_size,
								planes, w, NULL, h, flags) < 0){
		THROW_TJ("decompressing image");
		ret = -1;
	}
		
	tjDestroy(tjInstance);
	tjInstance = NULL;

	if (!ret) _interweave_yuv(Y, U, V, w, h, output);

	// free our temp memory
	free(memblock);
	return ret;
}

// this is special for our funky nv stereo frames
static int turbojpeg_read_stereo_image(char image_path[512],int w, int h, unsigned char* output){
	if (output == nullptr) return -1;

	int ret = 0;

	char* memblock;
	streampos file_size;
	std::ifstream input( image_path, std::ios::in|std::ios::binary|std::ios::ate );
	if (input.is_open()){
		file_size = input.tellg();
		memblock = new char [file_size];
		input.seekg (0, ios::beg);
		input.read (memblock, file_size);
		input.close();
	}
	else return -1;

	/* BEGIN JPEG COMPRESS */
	tjhandle tjInstance = NULL;

	unsigned char Y[w*h];
	unsigned char U[(w*h)/4];
	unsigned char V[(w*h)/4];

	unsigned char* planes[3] = { Y, U, V };
	int flags = 0;
	// speed up flags
	flags |= TJFLAG_FASTUPSAMPLE;
	flags |= TJFLAG_FASTDCT;

	if ((tjInstance = tjInitDecompress()) == NULL){
		THROW_TJ("initializing compressor");
		ret = -1;
	}

	if(!ret && tjDecompressToYUVPlanes(tjInstance, (unsigned char*)memblock, file_size,
								planes, w, NULL, h, flags) < 0){
		THROW_TJ("decompressing image");
		ret = -1;
	}
		
	tjDestroy(tjInstance);
	tjInstance = NULL;

	if (!ret) _interweave_yuv(Y, U, V, w, h/2, output);
	if (!ret) _interweave_yuv(Y+(w*h/2), U+(w*h/8), V+(w*h/8), w, h/2, output+(w*h*3/4));

	// free our temp memory
	free(memblock);
	return ret;
}

// todo, bundle up multiple imu samples since they are so fast.
static int _publish_vio(int ch, char* line)
{
	vio_data_t d;
	int ret;
	int index;
	int64_t timestamp_log;
	float rpy[3];
	float rpy_cam[3];
	int n_features, state;

	// scan data out of the line
	ret = sscanf(line, "%d,%ld,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%d,%d,%d,%d\n",\
	&index, &timestamp_log,\
	&d.T_imu_wrt_vio[0], &d.T_imu_wrt_vio[1], &d.T_imu_wrt_vio[2],\
	&rpy[0],&rpy[1],&rpy[2],\
	&d.vel_imu_wrt_vio[0], &d.vel_imu_wrt_vio[1], &d.vel_imu_wrt_vio[2],\
	&d.imu_angular_vel[0], &d.imu_angular_vel[1], &d.imu_angular_vel[2],\
	&d.gravity_vector[0], &d.gravity_vector[1], &d.gravity_vector[2],\
	&d.T_cam_wrt_imu[0], &d.T_cam_wrt_imu[1], &d.T_cam_wrt_imu[2],\
	&rpy_cam[0],&rpy_cam[1],&rpy_cam[2],\
	&n_features,&d.quality, &state, &d.error_code);

	// make sure all fields were populated
	if(ret!=27){
		fprintf(stderr, "failed to parse VIO csv line %d for channel %d, read %d values\n", c[ch].n_read+1, ch, ret);
		perror("error:");
		return -1;
	}

	// do the rotation calculations
	_tait_bryan_xyz_to_rotation_matrix(rpy, d.R_imu_to_vio);
	_tait_bryan_xyz_to_rotation_matrix(rpy_cam, d.R_cam_to_imu);

	// fill in the rest. features are state are not ints, so they are cast here
	d.magic_number = VIO_MAGIC_NUMBER;
	d.n_feature_points = n_features;
	d.state = state;

	// correct the timestamp to line up with current time and wait if necessary
	d.timestamp_ns = _correct_timestamp_ns(timestamp_log);
	_wait_before_publish(ch, d.timestamp_ns);

	// write to pipe!
	if(en_debug){
		printf("publishing to ch%d at %ldus a VIO packet with timestamp %ldus\n", ch, _time_monotonic_ns()/1000,d.timestamp_ns/1000);
	}
	pipe_server_write(ch, (char*)&d, sizeof(d));

	return 0;
}

// todo, bundle up multiple samples since they are so fast.
static int _publish_pose6DOF(int ch, char* line)
{
	pose_vel_6dof_t p;
	int ret;
	int index;
	int64_t timestamp_log;
	float rpy[3];

	// scan data out of the line
	ret = sscanf(line, "%d,%ld,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f\n",\
	&index, &timestamp_log,\
	&p.T_child_wrt_parent[0], &p.T_child_wrt_parent[1], &p.T_child_wrt_parent[2],\
	&rpy[0],&rpy[1],&rpy[2],\
	&p.v_child_wrt_parent[0], &p.v_child_wrt_parent[1], &p.v_child_wrt_parent[2],\
	&p.w_child_wrt_child[0], &p.w_child_wrt_child[1], &p.w_child_wrt_child[2]);

	// make sure all fields were populated
	if(ret!=14){
		fprintf(stderr, "failed to parse pose6DOF csv line %d for channel %d, read %d values\n", c[ch].n_read+1, ch, ret);
		perror("error:");
		return -1;
	}

	// do the rotation calculations
	_tait_bryan_xyz_to_rotation_matrix(rpy, p.R_child_to_parent);

	// fill in the rest. features are state are not ints, so they are cast here
	p.magic_number = POSE_VEL_6DOF_MAGIC_NUMBER;

	// correct the timestamp to line up with current time and wait if necessary
	p.timestamp_ns = _correct_timestamp_ns(timestamp_log);
	_wait_before_publish(ch, p.timestamp_ns);

	// write to pipe!
	if(en_debug){
		printf("publishing to ch%d at %ldus a pose6DOF packet with timestamp %ldus\n", ch, _time_monotonic_ns()/1000,p.timestamp_ns/1000);
	}
	pipe_server_write(ch, (char*)&p, sizeof(p));

	return 0;
}

// todo, bundle up multiple imu samples since they are so fast.
static int _publish_tof(int ch, char* line)
{
	tof_data_t d;
	int ret;
	int index;
	int64_t timestamp_log;

	// scan data out of the line
	ret = sscanf(line, "%d,%ld\n",\
	&index, &timestamp_log);

	// make sure all fields were populated
	if(ret!=2){
		fprintf(stderr, "failed to parse TOF csv line %d for channel %d, read %d values\n", c[ch].n_read+1, ch, ret);
		perror("error:");
		return -1;
	}

	// read from file
	ret = fread(&d, sizeof(d), 1, c[ch].raw_fd);
	if(ret!=1){
		perror("failed to read TOF raw file");
		return -1;
	}

	// correct the timestamp to line up with current time and wait if necessary
	d.timestamp_ns = _correct_timestamp_ns(timestamp_log);
	_wait_before_publish(ch, d.timestamp_ns);

	// write to pipe!
	if(en_debug){
		printf("publishing to ch%d at %ldus a TOF packet with timestamp %ldus\n", ch, _time_monotonic_ns()/1000,d.timestamp_ns/1000);
	}
	pipe_server_write(ch, (char*)&d, sizeof(d));

	return 0;
}

static void* _cam_prepper(void* arg){
	int ch = (long)arg;

	while (c[ch].running){
		if (c[ch].line_queue.empty()){
			usleep(1000);
			continue;
		}
		else {
			std::shared_ptr<cam_data> data_products{nullptr};

			pthread_mutex_lock(&c[ch].line_mutex);
			data_products = c[ch].line_queue.front();
			c[ch].line_queue.pop();
			pthread_mutex_unlock(&c[ch].line_mutex);

			// now need to create the image / images
			png_bytep buffer1;
			png_bytep buffer2;
			char img_path1[512];
			char img_path2[512];

			// big buffer, can handle regular nv images and the stereo case
			unsigned char nv_buffer[(data_products->meta.width * data_products->meta.height * 3)];

			// handle various formats
			if(data_products->meta.format == IMAGE_FORMAT_RAW8){
				_construct_cam_path(ch, data_products->meta.frame_id, img_path1);

				if (libpng_read_image(PNG_FORMAT_GRAY, 0, img_path1, buffer1) != 0){
					fprintf(stderr, "ERROR: failed to read png at %s\n", img_path1);
					continue;
				}

				data_products->meta.stride=data_products->meta.width;
				data_products->meta.size_bytes=data_products->meta.width*data_products->meta.height;

				data_products->meta.timestamp_ns = _correct_timestamp_ns(data_products->meta.timestamp_ns);

				_wait_before_publish(ch, data_products->meta.timestamp_ns+data_products->meta.exposure_ns);

				if(en_debug) printf("writing mono frame, %d bytes\n", data_products->meta.size_bytes);
				pipe_server_write_camera_frame(ch, data_products->meta, buffer1);
				free(buffer1);
			}
			else if(data_products->meta.format == IMAGE_FORMAT_STEREO_RAW8){
				_construct_stereo_path(ch, data_products->meta.frame_id, img_path1,img_path2);

				if (libpng_read_image(PNG_FORMAT_GRAY, 0, img_path1, buffer1) != 0){
					fprintf(stderr, "ERROR: failed to read png at %s\n", img_path1);
					continue;
				}
				if (libpng_read_image(PNG_FORMAT_GRAY, 0, img_path2, buffer2) != 0){
					fprintf(stderr, "ERROR: failed to read png at %s\n", img_path2);
					continue;
				}

				data_products->meta.stride=data_products->meta.width;
				data_products->meta.size_bytes=data_products->meta.width*data_products->meta.height*2;

				data_products->meta.timestamp_ns = _correct_timestamp_ns(data_products->meta.timestamp_ns);

				_wait_before_publish(ch, data_products->meta.timestamp_ns+data_products->meta.exposure_ns);

				// for stereo send both images
				if(en_debug) printf("writing stereo frame, %d bytes\n", data_products->meta.width*data_products->meta.height);
				pipe_server_write_stereo_frame(ch, data_products->meta, buffer1, buffer2);
				free(buffer1);
				free(buffer2);
			}
			else if(data_products->meta.format == IMAGE_FORMAT_RAW16){
				_construct_cam_path(ch, data_products->meta.frame_id, img_path1);

				if (libpng_read_image(PNG_FORMAT_LINEAR_Y, 1, img_path1, buffer1) != 0){
					fprintf(stderr, "ERROR: failed to read png at %s\n", img_path1);
					continue;
				}

				data_products->meta.stride=data_products->meta.width*2;
				data_products->meta.size_bytes=data_products->meta.width*data_products->meta.height*2;

				data_products->frame1 = (unsigned char*) malloc (data_products->meta.size_bytes);
				memcpy(data_products->frame1, buffer1, data_products->meta.size_bytes);

				// TODO: send this back out in a viewable format

				free(buffer1);
			}
			else if (data_products->meta.format == IMAGE_FORMAT_NV12 || data_products->meta.format == IMAGE_FORMAT_NV21) {
				_construct_jpg_cam_path(ch, data_products->meta.frame_id, img_path1);

				if (turbojpeg_read_image(img_path1, data_products->meta.width, data_products->meta.height, nv_buffer) != 0){
					fprintf(stderr, "ERROR: failed to read jpeg at %s\n", img_path1);
					continue;
				}

				data_products->meta.stride=data_products->meta.width*1.5;
				data_products->meta.size_bytes=data_products->meta.width*data_products->meta.height*1.5;

				data_products->meta.timestamp_ns = _correct_timestamp_ns(data_products->meta.timestamp_ns);

				_wait_before_publish(ch, data_products->meta.timestamp_ns+data_products->meta.exposure_ns);

				if(en_debug) printf("writing yuv frame, %d bytes\n", data_products->meta.size_bytes);
				pipe_server_write_camera_frame(ch, data_products->meta, nv_buffer);
			}
			else if (data_products->meta.format == IMAGE_FORMAT_STEREO_NV12 || data_products->meta.format == IMAGE_FORMAT_STEREO_NV21) {
				_construct_jpg_cam_path(ch, data_products->meta.frame_id, img_path1);

				if (turbojpeg_read_stereo_image(img_path1, data_products->meta.width, data_products->meta.height*2, nv_buffer) != 0){
					fprintf(stderr, "ERROR: failed to read jpeg at %s\n", img_path1);
					continue;
				}

				data_products->meta.stride=data_products->meta.width*1.5;
				data_products->meta.size_bytes=data_products->meta.width*data_products->meta.height*3;

				data_products->meta.timestamp_ns = _correct_timestamp_ns(data_products->meta.timestamp_ns);

				_wait_before_publish(ch, data_products->meta.timestamp_ns+data_products->meta.exposure_ns);

				if(en_debug) printf("writing stereo yuv frame, %d bytes\n", data_products->meta.size_bytes);
				pipe_server_write_camera_frame(ch, data_products->meta, nv_buffer);
			}
			else{
				fprintf(stderr, "ERROR only support RAW_8, STEREO_RAW8, NV12/21, and RAW_16 images right now\n");
				fprintf(stderr, "got %s instead\n", pipe_image_format_to_string(data_products->meta.format));
				// not sure what proper behavior should be here
				continue;
			}
			// 0 indexed
			if (data_products->meta.frame_id == c[ch].n_total-1)
				c[ch].running = 0;
		}
	}
	return NULL;
}

static void* _publisher(void* arg)
{
	//int ret;
	int ch = (long)arg;
	char* line = NULL;
	size_t buflen = 1024;
	ssize_t line_len;

	if(c[ch].type == TYPE_IMU){
		printf("starting to publish imu to: %s\n", c[ch].out_pipe_path);
	}
	else if(c[ch].type == TYPE_CAM){
		printf("starting to publish cam to: %s\n", c[ch].out_pipe_path);
	}
	else if(c[ch].type == TYPE_VIO){
		printf("starting to publish vio to: %s\n", c[ch].out_pipe_path);
	}
	else if(c[ch].type == TYPE_TOF){
		printf("starting to publish tof to: %s\n", c[ch].out_pipe_path);
	}
	else if (c[ch].type == TYPE_POSE_6DOF){
		printf("starting to publish pose6DOF to: %s\n", c[ch].out_pipe_path);
	}
	else{
		fprintf(stderr, "ERROR in publisher, unknown type %d\n", c[ch].type);
		return NULL;
	}

	// allocate a buffer for getline to read line into. getline will realloc
	// this buffer if its not big enough.
	line = (char*)malloc(buflen);
	if(line==NULL){
		perror("failed to allocate read buffer");
		c[ch].running = 0;
		return NULL;
	}

	// read in the csv header, this is a good test if the file is formed correctly
	line_len = getline(&line, &buflen, c[ch].csv_fd);
	if(line_len<10){
		perror("error reading csv header");
		c[ch].running = 0;
		return NULL;
	}
	if(en_debug){
		printf("header for channel %d %s\n", ch, c[ch].out_pipe_path);
		printf("%s", line);
	}
	c[ch].n_read++; // mark one line as read

	// keep running untill one of the many ending conditions is met:
	// main running is set to 0 by ctrl-c
	// c[ch].running is set to 0 by something
	// reach the end of the file
	// We don't look at the total samples recorded in the info.json file since
	// that may have not been updated correctly in even of a crash during logging
	// but the csv data may still be good and should be replayed anyway
	while(main_running && c[ch].running){
		// read the next line
		line_len = getline(&line, &buflen, c[ch].csv_fd);
		if(line_len<0){
			if(en_debug){
				printf("getline reached end of file for channel %d\n", ch);
			}
			// if its cam, we're pipelining so threads will need to do closure...
			if (c[ch].type != TYPE_CAM)
				c[ch].running = 0;
			return NULL;
		}
		c[ch].n_read++; // mark one line as read, not necessarily published yet

		// send line to appropriate publisher
		if(c[ch].type==TYPE_IMU)				_publish_imu(ch,line);
		else if(c[ch].type==TYPE_VIO)			_publish_vio(ch,line);
		else if(c[ch].type==TYPE_TOF)			_publish_tof(ch,line);
		else if (c[ch].type == TYPE_POSE_6DOF)	_publish_pose6DOF(ch, line);
		// pipelineeeeee
		else if(c[ch].type==TYPE_CAM){
			std::shared_ptr<cam_data> curr_cam = std::make_shared<cam_data>();

			// scan data out of the line
			int ret = sscanf(line, "%d,%ld,%hd,%d,%hd,%hd,%hd\n",\
			&curr_cam->meta.frame_id, &curr_cam->meta.timestamp_ns,\
			&curr_cam->meta.gain, &curr_cam->meta.exposure_ns, &curr_cam->meta.format, &curr_cam->meta.height, &curr_cam->meta.width);
			curr_cam->meta.magic_number = CAMERA_MAGIC_NUMBER;
			curr_cam->meta.reserved = 0;

			// make sure all fields were populated
			if(ret!=7){
				fprintf(stderr, "failed to parse CAM csv line %d for channel %d\n", c[ch].n_read+1, ch);
				perror("error:");
				continue;
			}

			pthread_mutex_lock(&c[ch].line_mutex);
			c[ch].line_queue.push(curr_cam);
			pthread_mutex_unlock(&c[ch].line_mutex);
		}
	}
	return NULL;
}


int main(int argc, char* argv[])
{
	int i;
	cJSON* parent;
	cJSON* channel_array;

	// start by parsing arguments
	if(_parse_opts(argc, argv)) return -1;

	// start signal handler so we can exit cleanly
	if(enable_signal_handler()==-1){
		fprintf(stderr,"ERROR: failed to start signal handler\n");
		return -1;
	}

	// try to open json info file
	char info_path[512];
	sprintf(info_path, "%sinfo.json", log_dir);
	if(en_debug) printf("opening json info file: %s\n", info_path);
	parent = json_read_file(info_path);
	if(parent==NULL){
		fprintf(stderr, "ERROR, couldn't find log at %s\n", log_dir);
		return -1;
	}

	// check format version
	int version;
	if(json_fetch_int(parent, "log_format_version", &version)){
		fprintf(stderr, "ERROR failed to find log_format_version in info.json\n");
		return -1;
	}
	if(en_debug) printf("using log_format_version=%d\n", version);

	// get number of channels in the log
	if(json_fetch_int(parent, "n_channels", &n_ch)){
		fprintf(stderr, "ERROR failed to find n_channels in info.json\n");
		return -1;
	}
	if(en_debug) printf("log contains %d channels\n", n_ch);

	// get start time of log
	// note cJSON uses 32-bit integers so we need to treat it as a double then
	// convert to 64-bit int.
	double log_start_time_double;
	if(json_fetch_double(parent, "start_time_monotonic_ns", &log_start_time_double)){
		fprintf(stderr, "ERROR failed to find start_time_monotonic_ns in info.json\n");
		return -1;
	}
	log_start_time_ns = log_start_time_double;
	if(en_debug) printf("log started at %ldns\n", log_start_time_ns);

	// grab the array of channels
	int array_len;
	channel_array = json_fetch_array(parent, "channels", &array_len);
	if(channel_array==NULL){
		fprintf(stderr, "ERROR failed to find channels array in info.json\n");
		return -1;
	}
	if(array_len!=n_ch){
		fprintf(stderr, "ERROR, log info.json claims to have %d channels but found %d\n", n_ch, array_len);
		return -1;
	}

	// load in data from each channel entry
	for(i=0;i<n_ch;i++){
		cJSON* array_item = cJSON_GetArrayItem(channel_array, i);

		if(array_item==NULL){
			fprintf(stderr, "ERROR, can't find channel %d json channel array\n", i);
		}
		if(json_fetch_int(array_item, "type", &c[i].type)){
			fprintf(stderr, "ERROR failed to find type for channel %d in info.json\n", i);
			return -1;
		}
		if(json_fetch_string(array_item, "pipe_path", c[i].out_pipe_path, MODAL_PIPE_MAX_PATH_LEN)){
			fprintf(stderr, "ERROR failed to find pipe_path for channel %d in info.json\n", i);
			return -1;
		}
		if(json_fetch_int(array_item, "n_samples", &c[i].n_total)){
			fprintf(stderr, "ERROR failed to find n_samples for channel %d in info.json\n", i);
			return -1;
		}
		// construct the path to the data for that channel, this will contain
		// the csv file and any other data/images for that channel
		sprintf(c[i].log_pipe_path,"%s%s", log_dir, c[i].out_pipe_path);

		// print this data out in debug mode
		if(en_debug){
			printf("log channel %d:\n", i);
			printf("    type: %s\n", _type_to_string(c[i].type));
			printf("    out path: %s\n", c[i].out_pipe_path);
			printf("    log path: %s\n", c[i].log_pipe_path);
			printf("    total samples: %d\n", c[i].n_total);
		}
	}

	// open all csv files
	for(i=0;i<n_ch;i++){
		char csv_path[512];
		sprintf(csv_path,"%sdata.csv", c[i].log_pipe_path);
		if(en_debug) printf("opening csv file: %s\n", csv_path);
		c[i].csv_fd = fopen(csv_path, "r");
		if(c[i].csv_fd==NULL){
			fprintf(stderr,"failed to open csv file: %s\n", csv_path);
			perror("error");
			return -1;
		}
	}


	// open all raw files
	for(i=0;i<n_ch;i++){
		if(c[i].type!=TYPE_TOF) continue;
		char raw_path[512];
		sprintf(raw_path,"%sdata.raw", c[i].log_pipe_path);
		if(en_debug) printf("opening raw file: %s\n", raw_path);
		c[i].raw_fd = fopen(raw_path, "r");
		if(c[i].raw_fd==NULL){
			fprintf(stderr,"failed to open raw file: %s\n", raw_path);
			perror("error");
			return -1;
		}
	}

	// check all the server channels, most likely what we will find is that the
	// user already has voxl-camera-server or voxl-imu-server running and will
	// need to stop these first, so we check if the pipes exist already
	for(i=0;i<n_ch;i++){
		if(pipe_exists(c[i].out_pipe_path)){
			printf("WARNING pipe already exists: %s\n", c[i].out_pipe_path);
			printf("You need to make sure the server that created it is stopped.\n");
			printf("would you like voxl-replay to do this now automatically?\n");
			printf("Press ENTER to kill server automatically, or ctrl-C to quit\n");
			if(continue_or_quit()){
				return -1;
			}
			printf("attemping to stop server that created %s\n", c[i].out_pipe_path);
			pipe_kill_server_process(c[i].out_pipe_path, 6.0);
		}
	}

	// now open up server channels
	for(i=0;i<n_ch;i++){

		pipe_info_t info;
		strcpy(info.location, c[i].out_pipe_path);
		strcpy(info.server_name, "voxl-replay");

		// trim name out of location, TODO have logger save the name too
		// find the start of the pipe name
		int dirlen = strlen(info.location);
		int start = 0;
		for(int i=dirlen-3;i>=0;i--){
			if(info.location[i]=='/'){
				start = i+1;
				break;
			}
		}
		if(start>0){
			memcpy(info.name, &info.location[start], dirlen-start-1);
			info.name[dirlen-start-1] = 0;
		}
		else{
			strcpy(info.name, info.location);
		}

		// fill in size and type
		switch(c[i].type) {
			case TYPE_IMU:
				info.size_bytes = IMU_RECOMMENDED_PIPE_SIZE;
				strcpy(info.type, "imu_data_t");
				break;
			case TYPE_CAM:
				info.size_bytes = 64*1024*1024;
				strcpy(info.type, "camera_image_metadata_t");
				break;
			case TYPE_VIO:
				info.size_bytes = VIO_RECOMMENDED_PIPE_SIZE;
				strcpy(info.type, "imu_data_t");
				break;
			case TYPE_TOF:
				info.size_bytes = TOF_RECOMMENDED_PIPE_SIZE;
				strcpy(info.type, "tof_data_t");
				break;
			case TYPE_POSE_6DOF:
				info.size_bytes = POSE_6DOF_RECOMMENDED_PIPE_SIZE;
				strcpy(info.type, "pose_vel_6dof_t");
				break;
			default:
				fprintf(stderr, "ERROR unknown log type: %d\n", c[i].type);
				pipe_server_close_all();
				return -1;
		}

		// no flags, we can't simulate a control pipe here
		int flags = 0;

		printf("creating pipe: %s\n", info.location);
		if(pipe_server_create(i, info, flags)){
			fprintf(stderr, "failed to start server for %s\n", c[i].out_pipe_path);
			pipe_server_close_all();
			return -1;
		}
	}

	// set main running flag to 1 so the threads about to be created don't just
	// exit right away
	main_running = 1;

	// record the current time, used for tweaking output timestamps
	replay_start_time_ns = _time_monotonic_ns();
	if(en_debug){
		printf("replay start time ns: %ld", replay_start_time_ns);
	}

	// start all the publisher threads
	for(i=0;i<n_ch;i++){
		c[i].running=1;
		pthread_create(&c[i].thread_id, NULL, _publisher, (void*)(long)i);
		if (c[i].type == TYPE_CAM){
			pthread_create(&c[i].cam_prep_id, NULL, _cam_prepper, (void*)(long)i);
		}
	}


	if(en_debug) printf("entering main loop\n");
	while(main_running){

		// check for the condition where all the threads stopped themselves
		int have_all_stopped = 1;
		for(i=0;i<n_ch;i++){
			if(c[i].running!=0){
				have_all_stopped = 0;
				break;
			}
		}
		if(have_all_stopped){
			if(en_debug) printf("all helpers stopped themselves\n");
			break;
		}

		// wait a bit
		usleep(100000);
	}


////////////////////////////////////////////////////////////////////////////////
// close everything
////////////////////////////////////////////////////////////////////////////////

   std::queue<std::shared_ptr<cam_data>> empty;

	// clear all queues
	for(i=0;i<n_ch;i++){
		if(!c[i].line_queue.empty()){
   			std::swap( c[i].line_queue, empty );
		}
	}

	// close all the pipes
	if(en_debug) printf("closing pipes\n");
	pipe_server_close_all();

	if(en_debug) printf("closing file descriptors\n");
	for(i=0;i<n_ch;i++){
		if(c[i].csv_fd!=0){
			fclose(c[i].csv_fd);
		}
		if(c[i].raw_fd!=0){
			fclose(c[i].raw_fd);
		}
	}

	printf("exiting cleanly\n");
	return 0;
}

